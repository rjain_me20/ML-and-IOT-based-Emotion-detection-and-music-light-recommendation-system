# ML-and-IOT-based-Emotion-detection-and-music-light-recommendation-system

### 1.Create Virtual environment
1. Anaconda: https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/
2. Windows & linux: https://www.geeksforgeeks.org/creating-python-virtual-environment-windows-linux/

### 2. Download image and song Dataset
1. link: https://www.kaggle.com/msambare/fer2013
2. Keep the image dataset in "data" folder, copy and paste in "test" and "train" separately.
3. You can add any song by your choice inside the songs category folder.

### 3. Install libraries in Virtual environment
1. pip install -r requirements.txt
Note: We are assuming you activate the virtual environment and then you are installing all the libraries

### 4. To Train the project
1. python emotion_music.py --mode train
Note: If got error, like library not found, make sure you have activated the virtual environment

### 5. Run the project
1. python emotion_music.py --mode display


### Please fork and give star if you found this project helpful
